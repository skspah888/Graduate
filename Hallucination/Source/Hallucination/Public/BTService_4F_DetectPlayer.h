// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTService.h"
#include "BTService_4F_DetectPlayer.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTService_4F_DetectPlayer : public UBTService
{
	GENERATED_BODY()
	
public:
	UBTService_4F_DetectPlayer();

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
