// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_4F_Contact.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTService_4F_Contact : public UBTService
{
	GENERATED_BODY()
	
public:
	UBTService_4F_Contact();
	
protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSecond) override;

};
