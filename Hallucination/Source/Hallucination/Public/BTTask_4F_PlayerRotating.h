// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_4F_PlayerRotating.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_4F_PlayerRotating : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	class AHN_Player*	RotatingPlayer;
	class ATargetPoint* TargetPoint;
	
public:
	UBTTask_4F_PlayerRotating();

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
