// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Character.h"
#include "GenericTeamAgentInterface.h"
#include "HN_Player.generated.h"

class AHN_Item;

UENUM()
enum class HAND_TYPE : uint8 { RIGHT, LEFT, END };

DECLARE_DELEGATE_OneParam(FDele_Single_OneParam, FVector);

UCLASS()
class HALLUCINATION_API AHN_Player : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
	UCameraComponent* VRCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
	UCameraComponent* PCCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	class USceneComponent* VROrigin;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	class USceneComponent* PCOrigin;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	UAudioComponent*		Speaker;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio)
	class USoundCue*		PlayerWalkSoundCue;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UCameraShake> MyShake;

	UPROPERTY(VisibleAnywhere, Category = "Player")
	bool		IsVRMode = false;

	UPROPERTY(VisibleAnywhere, Category = "Player")
	bool		IsInteractObject = true;

	UPROPERTY(VisibleAnywhere, Category = "Player")
	bool		IsPlayerController = true;

	AHN_Item* Inventory[(int)HAND_TYPE::END];

	FDele_Single_OneParam Fuc_DeleSingle_OneParam;

private:
	/*UPROPERTY()
	class AHN_PlayerController* HNPlayerController;*/

	FGenericTeamId TeamId;
	float fRotate;

	//	공포 저항 수치
	int iMaxHp;
	int iHp;

public:
	AHN_Player();

protected:
	UFUNCTION(BlueprintCallable)
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	UFUNCTION(BlueprintCallable)
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UFUNCTION(BlueprintCallable)
	void	SetEnablePlayerController(bool bCheck);
	UFUNCTION(BlueprintCallable)
	void	SetPlayerInteract(bool bCheck);

	FGenericTeamId	GetGenericTeamId() const;
	void			InitPlayer();

public:
	UFUNCTION(BlueprintCallable)
	void Scamp_Shake();
	UFUNCTION(BlueprintCallable)
	void Scamp_ShakeEnd();
	UFUNCTION(BlueprintCallable)
	void PlayWalkSound(float fValue);

	

public:
	UFUNCTION(BlueprintCallable, Category = "PlayerInput")
	void		UpDown(float NewAxisValue);
	UFUNCTION(BlueprintCallable, Category = "PlayerInput")
	void		LeftRight(float NewAxisValue);
	UFUNCTION(BlueprintCallable, Category = "PlayerInput")
	void		LookUp(float NewAxisValue);
	UFUNCTION(BlueprintCallable, Category = "PlayerInput")
	void		Turn(float NewAxisValue);

public:			// Delegate Function
	UFUNCTION()
	void DeleFunc_SetPosition(FVector originPos);

public:
	void		RotationController(const FRotator& Rotator);
	void		RotationCharacter(const FRotator& Rotator);
	UFUNCTION(BlueprintCallable)
	void		Interact();

	UFUNCTION(BlueprintCallable)
	void		SetCameraActivate(bool bCheck);

	void SetItem(HAND_TYPE _hand, AHN_Item* _item);
	bool UseItem(FName _name);
	bool CheckItem(FName _name);
};
