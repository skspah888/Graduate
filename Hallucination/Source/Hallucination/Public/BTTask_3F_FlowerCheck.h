// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_3F_FlowerCheck.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_3F_FlowerCheck : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	bool		IsInit = false;
	float		DelayTime = 0.f;
	float		CurrentTime = 0.f;

public:
	UBTTask_3F_FlowerCheck();

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
