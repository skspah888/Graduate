// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_ChangePos.generated.h"

UENUM(BlueprintType)
enum class PATTERN_TYPE : uint8 { FIRST, SHAKE, MOVE };

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_ChangePos : public UBTTaskNode
{
	GENERATED_BODY()
	
private:

	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = Type, Meta = (AllowPrivateAccess = true))
	PATTERN_TYPE CurrentType;
	
public:
	UBTTask_ChangePos();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
