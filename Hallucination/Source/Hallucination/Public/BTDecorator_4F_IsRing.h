// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_4F_IsRing.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTDecorator_4F_IsRing : public UBTDecorator
{
	GENERATED_BODY()
	
public:
	UBTDecorator_4F_IsRing();

protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
