// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_3F_IsSound.generated.h"

UCLASS()
class HALLUCINATION_API UBTDecorator_3F_IsSound : public UBTDecorator
{
	GENERATED_BODY()
	
public:
	UBTDecorator_3F_IsSound();

protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
	
};
