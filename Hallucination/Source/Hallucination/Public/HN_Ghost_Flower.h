// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "HN_Ghost_Flower.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_Ghost_Flower : public AHN_Ghost
{
	GENERATED_BODY()
	
private:
	/*TArray<class AHN_Player*>			PlayerArray;*/
	TArray<class AHN_Player*>					PlayerArray;
	TArray<FVector>								OriginPositionArray;
	TArray<class AHN_FlowerLight*>				LightArray;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	class USoundCue*		FlowerSoundCue;

public:
	AHN_Ghost_Flower();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;

public:
	void		SetOriginPosition(FVector originPos);

public:
	UFUNCTION()
	void OnAudioEnd();

	UFUNCTION()
	void StartSound();

	UFUNCTION()
	void CheckPlayer();

	UFUNCTION()
	void ChangeLight(bool IsChange);


};
