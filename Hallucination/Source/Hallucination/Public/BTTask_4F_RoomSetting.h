// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_4F_RoomSetting.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_4F_RoomSetting : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_4F_RoomSetting();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
