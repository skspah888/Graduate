// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/PlayerController.h"
#include "HN_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
private:
	FInputModeGameOnly			GameInputMode;			// 게임플레이에만 입력 전달
	FInputModeUIOnly			UIInputMode;			// UI에만 입력 전달
	
public:
	AHN_PlayerController();

	virtual void PostInitializeComponents() override;
	virtual void OnPossess(APawn* aPawn) override;

protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

private:
	void		Character_Change();

public:
	void		SetPlayerControllerInput(bool bCheck);
};
