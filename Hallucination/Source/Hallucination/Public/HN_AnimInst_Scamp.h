// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Animation/AnimInstance.h"
#include "HN_AnimInst_Scamp.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UHN_AnimInst_Scamp : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UHN_AnimInst_Scamp();
	UFUNCTION(BlueprintCallable)
	void SetIsIdle(bool _IsIdle);

private:
	UPROPERTY(BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsIdle;
	
};
