// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Engine/TriggerVolume.h"
#include "HN_4F_TriggerVolume.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_4F_TriggerVolume : public ATriggerVolume
{
	GENERATED_BODY()

	
protected:
	virtual void BeginPlay() override;

public:
	AHN_4F_TriggerVolume();

	UFUNCTION()
	void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	UFUNCTION()
	void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);

private:
	int32				PlayerCount;
};
