// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_AIController.h"
#include "HN_AI_Drag.generated.h"

UCLASS()
class HALLUCINATION_API AHN_AI_Drag : public AHN_AIController
{
	GENERATED_BODY()
	
public:
	static const FName				IsArriveKey;
	static const FName				TargetKey;
	static const FName				NotTargetKey;
	static const FName				IsMoveKey;
	static const FName				IsRoomSettingKey;
	static const FName				TargetLocation;
	static const FName				IsContactKey;
	static const FName				IsOpenDoorKey;

public:
	AHN_AI_Drag();

public:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

public:
	void SetDragBlackBoard(FString BoardName, bool bCheck);
};
