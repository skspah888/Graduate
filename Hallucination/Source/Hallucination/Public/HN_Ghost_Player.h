// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "HN_Ghost_Player.generated.h"

UCLASS()
class HALLUCINATION_API AHN_Ghost_Player : public AHN_Ghost
{
	GENERATED_BODY()
	
public:	
	AHN_Ghost_Player();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ghost")
	class USceneComponent*				GhostRootComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ghost")
	class UHN_DissolveComponent*		DissolveComponent;
};
