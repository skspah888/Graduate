// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "AIController.h"
#include "HN_AIController.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_AIController : public AAIController
{
	GENERATED_BODY()
	
protected:
	UPROPERTY()
	class UBehaviorTree* BTAsset;

	UPROPERTY()
	class UBlackboardData* BBAsset;

public:
	AHN_AIController();
	AHN_AIController(const FObjectInitializer& ObjectInitializer);

	virtual void RunAI();
	virtual void StopAI();
};
