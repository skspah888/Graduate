// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Actor.h"
#include "HN_WayPoint.generated.h"

UCLASS()
class HALLUCINATION_API AHN_WayPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHN_WayPoint();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USceneComponent*	Root;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UBoxComponent*		BoxComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AHN_WayPoint*		NextWayPoint;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnActorEnter(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, 
		UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
