// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "HN_Ghost_Nurse.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_Ghost_Nurse : public AHN_Ghost
{
	GENERATED_BODY()
	
public:
	AHN_Ghost_Nurse();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AHN_WayPoint* NextWayPoint;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;
	
};
