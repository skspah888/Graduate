// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/Tasks/BTTask_PlaySound.h"
#include "BTTask_HN_PlaySound.generated.h"

//DECLARE_MULTICAST_DELEGATE(FOnAudioFinished);

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_HN_PlaySound : public UBTTask_PlaySound
{
	GENERATED_BODY()
	
private:
	UAudioComponent* AISpeaker;

public:
	UBTTask_HN_PlaySound();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
