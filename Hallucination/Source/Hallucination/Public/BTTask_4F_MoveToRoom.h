// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_4F_MoveToRoom.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_4F_MoveToRoom : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	class AHN_Player*	TargetPlayer;
	class AHN_Player*	NotTargetPlayer;
	FVector				NextLocation;
	FRotator			NextRotator;

	bool				IsCheck;
	
public:
	UBTTask_4F_MoveToRoom();

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
