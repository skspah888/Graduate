// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(Hallucination, Log, All);

#define LOG_CALLINFO (FString(__FUNCTION__) + TEXT("(") + FString::FromInt(__LINE__) + TEXT(")"))

#define LOG_S(Verbosity) UE_LOG(Hallucination, Verbosity, TEXT("%s"), *LOG_CALLINFO)

#define LOG_TEXT(Verbosity, Format, ...) UE_LOG(Hallucination, Verbosity, TEXT("%s %s"), *LOG_CALLINFO, *FString::Printf(Format, ##__VA_ARGS__))

// check나 ensure 어설션 대신 사용, 런타임 에러 시 붉은색 로그와 함수 반환
// 조건문 Expr이 참이 아닐 경우 로그가 붉게 뜬다.
#define ERROR_CHECK(Expr, ...) { if((Expr)) { LOG_TEXT(Error, TEXT("ASSERTION : %s"), TEXT("'"#Expr"'")); return __VA_ARGS__;} }

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,text)
#define printFString(text, fstring) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(text), fstring))

enum BONE_NAME
{
	BONE_R_SPINE,
	BONE_R_ARM,
	BONE_R_ELBOW,
	BONE_END
};

enum class DOOR_STATE { IDLE, OPEN, CLOSE };
