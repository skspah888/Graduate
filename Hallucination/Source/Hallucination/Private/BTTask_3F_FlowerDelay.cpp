// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_3F_FlowerDelay.h"
#include "HN_AI_Flower.h"
#include "HN_Ghost_Flower.h"

UBTTask_3F_FlowerDelay::UBTTask_3F_FlowerDelay()
{
	bNotifyTick = false;
	NodeName = TEXT("3F Flower Delay");
}

EBTNodeResult::Type UBTTask_3F_FlowerDelay::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	Cast<AHN_AI_Flower>(OwnerComp.GetAIOwner())->SetFlowerTime();

	Cast<AHN_Ghost_Flower>(OwnerComp.GetAIOwner()->GetPawn())->ChangeLight(false);

	return EBTNodeResult::Succeeded;
}
