// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Item.h"
#include "HN_Player.h"

AHN_Item::AHN_Item()
{
	PrimaryActorTick.bCanEverTick = false;

	bActive = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MESH"));

	RootComponent = Mesh;

	eType = ITEM_TYPE::END;
}

void AHN_Item::BeginPlay()
{
	Super::BeginPlay();
	
}

void AHN_Item::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHN_Item::GetItem_Implementation(AHN_Player* _player)
{
}

void AHN_Item::SetRenderDepth(bool bValue)
{
	TArray<UActorComponent*> Comps = GetComponentsByTag(UPrimitiveComponent::StaticClass(), TEXT("outline"));
	UPrimitiveComponent* Com = Cast<UPrimitiveComponent>(Comps[0]);
	Com->SetRenderCustomDepth(bValue);
}

