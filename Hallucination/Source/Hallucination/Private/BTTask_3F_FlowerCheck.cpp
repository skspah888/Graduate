// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_3F_FlowerCheck.h"
#include "HN_AI_Flower.h"
#include "HN_Ghost_Flower.h"

UBTTask_3F_FlowerCheck::UBTTask_3F_FlowerCheck()
{
	bNotifyTick = true;
	NodeName = TEXT("3F Flower Check");
}

EBTNodeResult::Type UBTTask_3F_FlowerCheck::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	if (!IsInit)
	{
		DelayTime = FMath::RandRange(3.f, 5.f);
		Cast<AHN_Ghost_Flower>(OwnerComp.GetAIOwner()->GetPawn())->ChangeLight(true);
		IsInit = true;
	}

	return EBTNodeResult::InProgress;
}

void UBTTask_3F_FlowerCheck::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);

	// 시간 누적
	CurrentTime += DeltaSeconds;

	// 플레이어 검사
	Cast<AHN_Ghost_Flower>(OwnerComp.GetAIOwner()->GetPawn())->CheckPlayer();

	// 시간 검사
	if (CurrentTime > DelayTime)
	{
		IsInit = false;
		CurrentTime = 0.f;
		DelayTime = 0.f;

		// 사운드 다시 재생하기
		Cast<AHN_Ghost_Flower>(OwnerComp.GetAIOwner()->GetPawn())->StartSound();
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
