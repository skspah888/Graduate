// Fill out your copyright notice in the Description page of Project Settings.

#include "HN_Gate.h"

#define OPEN_Z 180.f

// Sets default values
AHN_Gate::AHN_Gate()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DOOR"));		//	문 열고 닫히는 부분
	Frame = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FRAME"));	// 문 틀

	RootComponent = Frame;
	Door->SetupAttachment(RootComponent);

	Frame->Mobility = EComponentMobility::Static;

	//	틀 초기화
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_FRAME(TEXT("StaticMesh'/Game/StarterContent/Props/SM_DoorFrame.SM_DoorFrame'"));

	if (SM_FRAME.Succeeded())
	{
		Frame->SetStaticMesh(SM_FRAME.Object);
	}

	//	문 초기화
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_DOOR(TEXT("StaticMesh'/Game/StarterContent/Props/SM_Door.SM_Door'"));

	if (SM_DOOR.Succeeded())
	{
		Door->SetStaticMesh(SM_DOOR.Object);
	}

	Door->SetRelativeLocation(FVector(0.f, 45.f, 0.f));

	bOpen = false;
	fValue = 0.f;
}

// Called when the game starts or when spawned
void AHN_Gate::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickEnabled(false);
}

// Called every frame
void AHN_Gate::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (fValue >= OPEN_Z)
	{
		SetActorTickEnabled(false);
		bOpen = true;
		return;
	}

	fValue += 0.2f;

	Door->SetRelativeLocation(FVector(0.f, 45.f, fValue));
}

void AHN_Gate::Open()
{
	SetActorTickEnabled(true);
}

