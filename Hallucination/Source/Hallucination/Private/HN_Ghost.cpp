// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost.h"

AHN_Ghost::AHN_Ghost()
{

}

void AHN_Ghost::Init()
{
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));
	Speaker->SetupAttachment(RootComponent);
}

void AHN_Ghost::BeginPlay()
{
	Super::BeginPlay();
}

void AHN_Ghost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ghost::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
}

void AHN_Ghost::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}