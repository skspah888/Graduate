// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Object.h"

// Sets default values
AHN_Object::AHN_Object()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
		
	bActive = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MESH"));

	RootComponent = Mesh;
}

// Called when the game starts or when spawned
void AHN_Object::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHN_Object::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHN_Object::ActiveOn_Implementation()
{
}

void AHN_Object::ActiveOff_Implementation()
{
}

void AHN_Object::SetRenderDepth(bool bValue)
{
	TArray<UActorComponent*> Comps = GetComponentsByTag(UPrimitiveComponent::StaticClass(), TEXT("outline"));
	UPrimitiveComponent* Com = Cast<UPrimitiveComponent>(Comps[0]);
	Com->SetRenderCustomDepth(bValue);
}

