// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_GetTargetPoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_AI_Drag.h"
#include "HN_Ghost_Drag.h"

UBTTask_4F_GetTargetPoint::UBTTask_4F_GetTargetPoint()
{
	NodeName = TEXT("Set Target Location");
}

EBTNodeResult::Type UBTTask_4F_GetTargetPoint::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	bool IsEnd = OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Drag::IsMoveKey);

	if(IsEnd)
		return EBTNodeResult::Succeeded;

	// BlackBoard에 Player가 이동할 목표위치 Set
	auto GhostDrag = Cast<AHN_Ghost_Drag>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == GhostDrag)
		return EBTNodeResult::Failed;

	FVector TargetLocation = FVector::ZeroVector;
	TargetLocation = GhostDrag->Get_TargetLocation();
	
	OwnerComp.GetBlackboardComponent()->SetValueAsVector(AHN_AI_Drag::TargetLocation, TargetLocation);

	return EBTNodeResult::Succeeded;
}
