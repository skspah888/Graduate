// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Player.h"
#include "HN_DissolveComponent.h"

// Sets default values
AHN_Ghost_Player::AHN_Ghost_Player()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ������Ʈ �ʱ�ȭ
	GhostRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	DissolveComponent = CreateDefaultSubobject<UHN_DissolveComponent>(TEXT("DissolveComponent"));


	// ���̷�Ż �޽�
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_PLAYER(TEXT(
		"SkeletalMesh'/Game/Asset/NewAsset/4F_Model/Player/Player1/Player1.Player1'"));
	if (SK_PLAYER.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_PLAYER.Object);
	}

	RootComponent = GetCapsuleComponent();
	GetMesh()->SetupAttachment(RootComponent);

	// ĸ�� ������Ʈ
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("HNCharacter"));
}

// Called when the game starts or when spawned
void AHN_Ghost_Player::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AHN_Ghost_Player::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

