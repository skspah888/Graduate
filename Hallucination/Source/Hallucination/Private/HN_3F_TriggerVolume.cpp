// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_3F_TriggerVolume.h"
#include "DrawDebugHelpers.h"
#include "HN_Ghost_Flower.h"
#include "Engine/TargetPoint.h"
#include "EngineUtils.h"

AHN_3F_TriggerVolume::AHN_3F_TriggerVolume()
{
	OnActorBeginOverlap.AddDynamic(this, &AHN_3F_TriggerVolume::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AHN_3F_TriggerVolume::OnOverlapEnd);
}

void AHN_3F_TriggerVolume::BeginPlay()
{
	DrawDebugBox(GetWorld(), GetActorLocation(), GetActorScale() * 100, FColor::Cyan, true, -1, 0, 5);
}

void AHN_3F_TriggerVolume::OnOverlapBegin(AActor * OverlappedActor, AActor * OtherActor)
{
	if (OtherActor && (OtherActor != this)) {
		//print("Overlap Begin");
		//printFString("Other Actor = %s", *OtherActor->GetName());

		if (TargetPoint == nullptr)
			return;
		FActorSpawnParameters SpawnInfo;
		AHN_Ghost_Flower* Ghost = GetWorld()->SpawnActor<AHN_Ghost_Flower>(AHN_Ghost_Flower::StaticClass(), TargetPoint->GetActorLocation(), OtherActor->GetActorRotation(), SpawnInfo);
		Ghost->SetOriginPosition(GetActorLocation());
		
		Destroy();
	}
}

void AHN_3F_TriggerVolume::OnOverlapEnd(AActor * OverlappedActor, AActor * OtherActor)
{
	if (OtherActor && (OtherActor != this)) {
		//print("Overlap Ended");
		//printFString("%s has left the Trigger Volume", *OtherActor->GetName());
	}
}