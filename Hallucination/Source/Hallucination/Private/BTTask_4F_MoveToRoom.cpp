// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_MoveToRoom.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Ghost_Drag.h"
#include "HN_AI_Drag.h"
#include "HN_Player.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "HN_PlayerController.h"


UBTTask_4F_MoveToRoom::UBTTask_4F_MoveToRoom()
{
	bNotifyTick = true;
	NodeName = TEXT("Move To Location");
	IsCheck = false;
}

EBTNodeResult::Type UBTTask_4F_MoveToRoom::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	// 움직여야할 타겟을 받아온다
	TargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::TargetKey));
	if (nullptr == TargetPlayer)
		return EBTNodeResult::Failed;

	NotTargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::NotTargetKey));
	if (nullptr == NotTargetPlayer)
		return EBTNodeResult::Failed;

	// 컨트롤러 끊기
	Cast<AHN_Player>(TargetPlayer)->SetEnablePlayerController(false);

	// 목표 타겟 Vector
	NextLocation = OwnerComp.GetBlackboardComponent()->GetValueAsVector(AHN_AI_Drag::TargetLocation);

	NextRotator = TargetPlayer->GetActorRotation();

	return EBTNodeResult::InProgress;
}

void UBTTask_4F_MoveToRoom::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);

	// 추적 벡터 구하기
	FVector LookVector = (NotTargetPlayer->GetActorLocation() - TargetPlayer->GetActorLocation());
	LookVector.Z = 0.f;
	FRotator TargetRot = FRotationMatrix::MakeFromX(LookVector).Rotator();

	// 지정한 속도만큼 회전
	TargetPlayer->RotationCharacter(FMath::RInterpTo(TargetPlayer->GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 1.6f));
	TargetPlayer->RotationController(FMath::RInterpTo(TargetPlayer->GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 1.6f));

	//TargetPlayer->RotationCharacter(FMath::RInterpTo(NextRotator, TargetRot, GetWorld()->GetDeltaSeconds(), 1.6f));
	//TargetPlayer->RotationController(FMath::RInterpTo(NextRotator, TargetRot, GetWorld()->GetDeltaSeconds(), 1.6f));

	// 캐릭터 이동
	//Cast<AHN_PlayerController>(TargetPlayer->GetController())->MoveToActor()

	UAIBlueprintHelperLibrary::SimpleMoveToLocation(TargetPlayer->GetController(), NextLocation);
	

	FVector PlayerLocation = TargetPlayer->GetActorLocation();
	PlayerLocation.Z = NextLocation.Z;
	FVector Distance = PlayerLocation - NextLocation;
	float fDist = Distance.Size();

	/*FString strName = "Dist : " + FString::FromInt(fDist);
	print(strName);*/

	if (fDist < 50.f)
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
