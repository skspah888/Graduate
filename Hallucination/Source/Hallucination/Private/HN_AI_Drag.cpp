// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AI_Drag.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

const FName AHN_AI_Drag::IsArriveKey(TEXT("IsArrive"));
const FName AHN_AI_Drag::TargetKey(TEXT("DragTarget"));
const FName AHN_AI_Drag::NotTargetKey(TEXT("DragNotTarget"));
const FName AHN_AI_Drag::IsMoveKey(TEXT("IsMove"));
const FName AHN_AI_Drag::IsRoomSettingKey(TEXT("IsRoomSetting"));
const FName AHN_AI_Drag::TargetLocation(TEXT("RoomTargetLocation"));
const FName AHN_AI_Drag::IsOpenDoorKey(TEXT("IsOpen"));
const FName AHN_AI_Drag::IsContactKey(TEXT("IsContact"));

AHN_AI_Drag::AHN_AI_Drag()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT(
		"BlackboardData'/Game/AI/4F/BB_4F.BB_4F'"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;		// 블랙보드 셋팅
	}

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT(
		"BehaviorTree'/Game/AI/4F/BT_4F_Drag.BT_4F_Drag'"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;		// 비헤이비어트리 셋팅
	}
}

void AHN_AI_Drag::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	// 블랙보드 사용
	UseBlackboard(BBAsset, Blackboard);
	// 비헤이비어 트리 사용
	RunBehaviorTree(BTAsset);
}

void AHN_AI_Drag::OnUnPossess()
{
	Super::OnUnPossess();
}

void AHN_AI_Drag::SetDragBlackBoard(FString BoardName, bool bCheck)
{
	if (BoardName == "Open")
	{
		GetBlackboardComponent()->SetValueAsBool(IsOpenDoorKey, bCheck);
	}
	else if (BoardName == "Contact")
	{
		GetBlackboardComponent()->SetValueAsBool(IsContactKey, bCheck);
	}
}
