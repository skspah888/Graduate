// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Nurse.h"
#include "HN_AI_Nurse.h"

AHN_Ghost_Nurse::AHN_Ghost_Nurse()
{
	PrimaryActorTick.bCanEverTick = false;

	// ���̷�Ż �޽� : �׽�Ʈ��
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_NURSE(TEXT("SkeletalMesh'/Game/Characters/Scamp/scampani_idle.scampani_idle'"));
	if (SK_NURSE.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_NURSE.Object);
	}

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 360.f, 0.f);
	GetCharacterMovement()->MaxWalkSpeed = 150.f;
	GetMesh()->SetRelativeRotation(FRotator(0.f, -90.f, 0.f));

	// AI ����
	AIControllerClass = AHN_AI_Nurse::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}


void AHN_Ghost_Nurse::BeginPlay()
{
	Super::BeginPlay();
}

void AHN_Ghost_Nurse::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ghost_Nurse::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);
}