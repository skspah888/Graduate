// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AIController.h"

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

AHN_AIController::AHN_AIController()
{
	BTAsset = nullptr;
	BBAsset = nullptr;
}

AHN_AIController::AHN_AIController(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void AHN_AIController::RunAI()
{
}

void AHN_AIController::StopAI()
{
	auto BehaviorTreeComponent = Cast<UBehaviorTreeComponent>(BrainComponent);
	if (nullptr != BehaviorTreeComponent)
	{
		BehaviorTreeComponent->StopTree(EBTStopMode::Safe);
	}
}
