// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_DissolveComponent.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UHN_DissolveComponent::UHN_DissolveComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	isDoingDissolve = false;
	CurrentDissolveAmount = 0.f;
	AccumulatedTime = 0.f;
	DissolveColor = FLinearColor(1.f, 0.f, 0.9f, 1.f);
	FinishDissolve = false;

	/*static ConstructorHelpers::FClassFinder<UCurveFloat> CurveFloat(TEXT(
		"CurveFloat'/Game/Materials/Dissolve2/DissolveCurve.DissolveCurve'"));
	if (CurveFloat.Succeeded() && CurveFloat.Class != NULL)
	{
		DissolveCurve = CurveFloat.Class;
	}*/
}


// Called when the game starts
void UHN_DissolveComponent::BeginPlay()
{
	Super::BeginPlay();

	GetSkeletalMeshes();
	GetStaticMeshes();
	SetColor();
}


// Called every frame
void UHN_DissolveComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!isDoingDissolve)
		return;

	AccumulatedTime += DeltaTime;

	CurrentDissolveAmount = Cast<UCurveFloat>(DissolveCurve)->GetFloatValue(AccumulatedTime);

	isDoingDissolve = UKismetMathLibrary::Less_FloatFloat(CurrentDissolveAmount, 1.f);

	for (auto DissolveMaterial : DissolveMaterials)
	{
		FName ParameterName = "Dissolve Amount";
		Cast<UMaterialInstanceDynamic>(DissolveMaterial)->SetScalarParameterValue(ParameterName, CurrentDissolveAmount);
	}

	if (!isDoingDissolve)
		FinishDissolve = true;
}

void UHN_DissolveComponent::GetStaticMeshes()
{
	TArray<UActorComponent*> StaticMeshComponents = GetOwner()->GetComponentsByClass(UStaticMeshComponent::StaticClass());
	for (int i = 0; i < StaticMeshComponents.Num(); ++i)
	{
		TArray<UMaterialInterface*> MaterialInterfaces = Cast<UStaticMeshComponent>(StaticMeshComponents[i])->GetMaterials();
		for (int j = 0; j < MaterialInterfaces.Num(); ++j)
		{
			UMaterialInstanceDynamic* MaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), MaterialInterfaces[j]);
			Cast<UStaticMeshComponent>(StaticMeshComponents[i])->SetMaterial(j, MaterialInstance);
			DissolveMaterials.Add(MaterialInstance);
		}
	}
}

void UHN_DissolveComponent::GetSkeletalMeshes()
{
	TArray<UActorComponent*> SkeletalMeshComponents = GetOwner()->GetComponentsByClass(USkeletalMeshComponent::StaticClass());
	for (int i = 0; i < SkeletalMeshComponents.Num(); ++i)
	{
		TArray<UMaterialInterface*> MaterialInterfaces = Cast<USkeletalMeshComponent>(SkeletalMeshComponents[i])->GetMaterials();
		for (int j = 0; j < MaterialInterfaces.Num(); ++j)
		{
			UMaterialInstanceDynamic* MaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), MaterialInterfaces[j]);
			Cast<USkeletalMeshComponent>(SkeletalMeshComponents[i])->SetMaterial(j, MaterialInstance);
			DissolveMaterials.Add(MaterialInstance);
		}
	}
}

void UHN_DissolveComponent::SetColor()
{
	for (auto DissolveMaterial : DissolveMaterials)
	{
		FName ParameterName = "Dissolve Color";
		Cast<UMaterialInstanceDynamic>(DissolveMaterial)->SetVectorParameterValue(ParameterName, DissolveColor);
	}
}

void UHN_DissolveComponent::ResetDissolve()
{
	isDoingDissolve = false;
	CurrentDissolveAmount = 0.f;
	AccumulatedTime = 0.f;
	for (auto DissolveMaterial : DissolveMaterials)
	{
		FName ParameterName = "Dissolve Amount";
		Cast<UMaterialInstanceDynamic>(DissolveMaterial)->SetScalarParameterValue(ParameterName, 0.f);
	}
}

void UHN_DissolveComponent::DoDissolve()
{
	isDoingDissolve = true;
}

