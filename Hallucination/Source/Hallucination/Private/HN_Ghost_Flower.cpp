// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Flower.h"
#include "Engine/TargetPoint.h"
#include "EngineUtils.h"
#include "HN_Player.h"
#include "HN_AI_Flower.h"
#include "Sound/SoundCue.h"
#include "HN_FlowerLight.h"

AHN_Ghost_Flower::AHN_Ghost_Flower()
{
	PrimaryActorTick.bCanEverTick = false;

	// AI 설정
	AIControllerClass = AHN_AI_Flower::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundCue> FlowerSoundCueObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Flower_Cue.Flower_Cue'"));
	if (FlowerSoundCueObject.Succeeded())
	{
		FlowerSoundCue = FlowerSoundCueObject.Object;
		Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("FlowerAudioComponent"));
		Speaker->SetupAttachment(RootComponent);

		Speaker->OnAudioFinished.AddDynamic(this, &AHN_Ghost_Flower::OnAudioEnd);
	}
}

void AHN_Ghost_Flower::BeginPlay()
{
	Super::BeginPlay();
	

	// 플레이어 셋팅
	UWorld* CurrentWorld = GetWorld();
	for (TActorIterator<AHN_Player> Iter(CurrentWorld); Iter; ++Iter)
	{
		//print(*Iter->GetName());
		PlayerArray.Add(*Iter);
		OriginPositionArray.Add((*Iter)->GetActorLocation());
	}

	// 라이트 셋팅
	for (TActorIterator<AHN_FlowerLight> Iter(CurrentWorld); Iter; ++Iter)
	{
		LightArray.Add(*Iter);
	}

	// 사운드 셋팅
	if (FlowerSoundCue && Speaker)
	{
		Speaker->SetSound(FlowerSoundCue);
		StartSound();
	}
}

void AHN_Ghost_Flower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ghost_Flower::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);
}

void AHN_Ghost_Flower::SetOriginPosition(FVector originPos)
{
	// OriginPos 셋팅
	int dir = 1;
	for (int i = 0; i < OriginPositionArray.Num(); ++i)
	{
		FVector PlayerPosition = OriginPositionArray[i];
		PlayerPosition.X = originPos.X;
		PlayerPosition.Y = originPos.Y;

		float fRandom = FMath::RandRange(30.f, 100.f);
		PlayerPosition.Y = PlayerPosition.Y + dir * fRandom;
		dir = -dir;

		OriginPositionArray[i] = PlayerPosition;
	}
	
}

void AHN_Ghost_Flower::StartSound()
{
	// 사운드 재생 후 IsSound true 변경
	Speaker->SetPitchMultiplier(FMath::RandRange(1.f, 3.f));
	Speaker->Play();

	auto FlowerController = Cast<AHN_AI_Flower>(Controller);
	FlowerController->SoundCheck(true);
}

void AHN_Ghost_Flower::OnAudioEnd()
{
	print("Stop Sound!!");

	// 사운드가 끝났음 => IsSound  false 변경하기
	auto FlowerController = Cast<AHN_AI_Flower>(Controller);
	FlowerController->SoundCheck(false);
}

void AHN_Ghost_Flower::CheckPlayer()
{
	// 플레이어 움직임 검사
	for (int i = 0; i < PlayerArray.Num(); ++i)
	{
		FVector vecVelocity = PlayerArray[i]->GetVelocity();
		vecVelocity.Z = 0.f;
		float Length = vecVelocity.Size();

		if (Length != 0.f)
		{
			// 플레이어에 지정된 이벤트 실행하기
			// 파라미터로는 i번째에 있는 originpositon
			PlayerArray[i]->Fuc_DeleSingle_OneParam.Execute(OriginPositionArray[i]);
		}
	}
}

void AHN_Ghost_Flower::ChangeLight(bool IsChange)
{
	for (auto light : LightArray)
	{
		light->SetCustomLightColor(IsChange);
	}
}

