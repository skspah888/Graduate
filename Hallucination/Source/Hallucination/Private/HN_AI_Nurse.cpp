// Fill out your copyright notice in the Description page of Project Settings.

#include "HN_AI_Nurse.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISense.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Player.h"
#include "HN_Ghost_Nurse.h"
#include "HN_WayPoint.h"

AHN_AI_Nurse::AHN_AI_Nurse()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT(
		"BlackboardData'/Game/AI/3F/BB_3F.BB_3F'"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;		// 블랙보드 셋팅
	}

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT(
		"BehaviorTree'/Game/AI/3F/BT_3F_Nurse.BT_3F_Nurse'"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;		// 비헤이비어트리 셋팅
	}

	// AI Perception [ Sight ] 
	SetPerceptionComponent(*CreateOptionalDefaultSubobject<UAIPerceptionComponent>(TEXT("AI Perception")));
	SightConfig = CreateOptionalDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));

	// 팀 ID 설정
	SetGenericTeamId(FGenericTeamId(1));
}

AHN_AI_Nurse::AHN_AI_Nurse(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	Initialize_SightSense();
	GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AHN_AI_Nurse::OnTargetUpdate);
	GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &AHN_AI_Nurse::OnPerceptionUpdate);

	SetGenericTeamId(FGenericTeamId(1));
}

void AHN_AI_Nurse::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AHN_Ghost_Nurse* ControllerPawn = Cast<AHN_Ghost_Nurse>(GetPawn());

	if (DistanceToTarget > AISightRadius)
	{
		IsTargetDetected = false;
	}

	if (ControllerPawn->NextWayPoint != nullptr && !IsTargetDetected)
	{
		//EPathFollowingRequestResult::Type ResultType = MoveToActor(ControllerPawn->NextWayPoint, 1.f);

		//switch (ResultType)
		//{
		//case EPathFollowingRequestResult::Failed:
		//	print("Failed");
		//	break;
		//case EPathFollowingRequestResult::AlreadyAtGoal:
		//	print("AlreadyAtGoal");
		//	break;
		//case EPathFollowingRequestResult::RequestSuccessful:
		//	//print("RequestSuccessful");
		//	break;
		//default:
		//	break;
		//}
		MoveToActor(ControllerPawn->NextWayPoint, 1.f);
	}
	else if (IsTargetDetected)
	{
		ACharacter* Target = Cast<ACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		MoveToActor(Target, 1.f);
	}
}

void AHN_AI_Nurse::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	print(InPawn->GetName());

	// 블랙보드 사용
	UseBlackboard(BBAsset, Blackboard);
	// 비헤이비어 트리 사용
	RunBehaviorTree(BTAsset);

	// 블랙보드 사용
	//GetBlackboardComponent()->SetValueAsFloat(DelayTimeKey, RandTime);

}

void AHN_AI_Nurse::OnUnPossess()
{
	Super::OnUnPossess();
}

void AHN_AI_Nurse::Initialize_SightSense()
{
	// AI Perception [ Sight ] 
	SetPerceptionComponent(*CreateOptionalDefaultSubobject<UAIPerceptionComponent>(TEXT("AI Perception")));
	SightConfig = CreateOptionalDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));

	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightAge);

	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;

	GetPerceptionComponent()->ConfigureSense(*SightConfig);
	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
}

void AHN_AI_Nurse::OnTargetUpdate(AActor* Actor, FAIStimulus Stimulus)
{
	print(Actor->GetName());
}

void AHN_AI_Nurse::OnPerceptionUpdate(const TArray<AActor*>& DetectedPawns)
{
	for (size_t i = 0; i < DetectedPawns.Num(); i++)
	{
		DistanceToTarget = GetPawn()->GetDistanceTo(DetectedPawns[i]);

		UE_LOG(LogTemp, Warning, TEXT("Distance: %f"), DistanceToTarget);
	}

	IsTargetDetected = true;
}
