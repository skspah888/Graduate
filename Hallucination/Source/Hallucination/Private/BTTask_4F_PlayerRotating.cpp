// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_PlayerRotating.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Player.h"
#include "HN_AI_Drag.h"
#include "HN_Ghost_Drag.h"
#include "Engine/TargetPoint.h"
#include "HN_PlayerController.h"

UBTTask_4F_PlayerRotating::UBTTask_4F_PlayerRotating()
{
	bNotifyTick = true;
	NodeName = TEXT("Rotating Player");
}

EBTNodeResult::Type UBTTask_4F_PlayerRotating::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	RotatingPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::NotTargetKey));
	if (nullptr == RotatingPlayer)
		return EBTNodeResult::Failed;

	TargetPoint = Cast<ATargetPoint>(Cast<AHN_Ghost_Drag>(OwnerComp.GetAIOwner()->GetPawn())->Get_LookPoint());
	if (nullptr == TargetPoint)
		return EBTNodeResult::Failed;

	// 컨트롤러 끊기
	Cast<AHN_Player>(RotatingPlayer)->SetEnablePlayerController(false);

	return EBTNodeResult::InProgress;
}

void UBTTask_4F_PlayerRotating::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);

	// 추적 벡터 구하기
	FVector LookVector = (TargetPoint->GetActorLocation() - RotatingPlayer->GetActorLocation()).GetSafeNormal();
	LookVector.Z = 0.f;
	FRotator TargetRot = FRotationMatrix::MakeFromX(LookVector).Rotator();

	// 지정한 속도만큼 회전
	Cast<AHN_Player>(RotatingPlayer)->RotationController(FMath::RInterpTo(RotatingPlayer->GetActorRotation(), 
															TargetRot, GetWorld()->GetDeltaSeconds(), 1.6f));
	Cast<AHN_Player>(RotatingPlayer)->RotationCharacter(FMath::RInterpTo(RotatingPlayer->GetActorRotation(), 
															TargetRot, GetWorld()->GetDeltaSeconds(), 1.6f));

	if (OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Drag::IsMoveKey))
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
