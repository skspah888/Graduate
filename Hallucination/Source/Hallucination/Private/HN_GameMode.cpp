// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_GameMode.h"
#include "HN_Player.h"
#include "HN_PlayerController.h"
#include "HN_Ghost_Drag.h"

AHN_GameMode::AHN_GameMode()
{
	// 플레이어 입장시 해당 클래스 정보를 기반으,로 폰 생성
	DefaultPawnClass = AHN_Player::StaticClass();
	PlayerControllerClass = AHN_PlayerController::StaticClass();
}

void AHN_GameMode::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AHN_GameMode::PostLogin(APlayerController * NewPlayer)
{
	Super::PostLogin(NewPlayer);
}

void AHN_GameMode::BeginPlay()
{
	////// 테스트 귀신 생성
	//FTransform SpawnLocation;
	//SpawnLocation.SetLocation(FVector(0, 0, 200));
	//GetWorld()->SpawnActor<AHN_Ghost_Drag>(AHN_Ghost_Drag::StaticClass(), SpawnLocation);
}
