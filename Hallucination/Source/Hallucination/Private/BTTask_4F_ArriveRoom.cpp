// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_ArriveRoom.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_AI_Drag.h"
#include "HN_Player.h"
#include "HN_Ghost_Drag.h"

UBTTask_4F_ArriveRoom::UBTTask_4F_ArriveRoom()
{
	NodeName = TEXT("Arrive Player, Room Setting");
}

EBTNodeResult::Type UBTTask_4F_ArriveRoom::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	// Arrive True
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Drag::IsArriveKey, true);

	// IsMove false 
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Drag::IsMoveKey, false);
	
	// Roomsetting false
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Drag::IsRoomSettingKey, false);

	// 컨트롤러 모두 EnableInput
	auto TargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::TargetKey));
	if (nullptr == TargetPlayer)
		return EBTNodeResult::Failed;

	auto NotTargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::NotTargetKey));
	if (nullptr == NotTargetPlayer)
		return EBTNodeResult::Failed;

	TargetPlayer->SetEnablePlayerController(true);
	NotTargetPlayer->SetEnablePlayerController(true);

	// TargetPlayer는 상호작용 못하게
	TargetPlayer->SetPlayerInteract(false);

	// Door Close
	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	Cast<AHN_Ghost_Drag>(ControllingPawn)->Setting_Door(false);

	Cast<AHN_Ghost_Drag>(ControllingPawn)->SetActorIgnore(TargetPlayer, false);


	return EBTNodeResult::Succeeded;
}
