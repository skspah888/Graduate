// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "SensorFusionBPLibrary.h"
#include "SensorFusionEngine.h"

SensorFusionEngine gSensorFusion;


USensorFusionBPLibrary::USensorFusionBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

//Connect
bool USensorFusionBPLibrary::Connect()
{
	//UE_LOG(LogTemp, Warning, TEXT("++++++ USensorFusionBPLibrary::Connect()"));

	gSensorFusion.Connect();

	return true;
}

void USensorFusionBPLibrary::Disconnect()
{
	UE_LOG(LogTemp, Warning, TEXT("++++++ USensorFusionBPLibrary::Disconnect()"));

	gSensorFusion.Disconnect();

	return;
}

bool USensorFusionBPLibrary::IsConnected()
{
	return gSensorFusion.IsConnected();
}

void USensorFusionBPLibrary::GetSensorRotation(int idxSensor, FRotator & Rotation, bool bRawData)
{
	gSensorFusion.GetSensorRotation(idxSensor, Rotation, bRawData);
}

bool USensorFusionBPLibrary::BindRotation(int idxSensor)
{
	return gSensorFusion.BindRotation(idxSensor);
}

bool USensorFusionBPLibrary::IsRotationBinded(int idxSensor)
{
	return gSensorFusion.IsRotationBinded(idxSensor);
}

void USensorFusionBPLibrary::PrintLog()
{
	UE_LOG(LogSensorFusion, Warning, TEXT("이 플러그인은, 한국산업기술대 게임공학과 이택희 교수님 연구팀에서 제작하였습니다."));
}

void USensorFusionBPLibrary::GetSensorAcceleration(int idxSensor, FVector & acceleration, bool bRawData)
{
	gSensorFusion.GetSensorAccel(idxSensor, acceleration, bRawData);
}

void USensorFusionBPLibrary::GetSensorGyro(int idxSensor, FVector & gyro, bool bRawData)
{
	gSensorFusion.GetSensorGyro(idxSensor, gyro, bRawData);
}

void USensorFusionBPLibrary::GetSensorBattery(int idxSensor, int& battery, bool bRawData)
{
	gSensorFusion.GetSensorBattery(idxSensor, battery, bRawData);
}

void USensorFusionBPLibrary::ReadData()
{
    gSensorFusion.ReadData();
}
