// Fill out your copyright notice in the Description page of ProjectSen Settings.

#include "SensorData.h"

SensorData::SensorData()
{
	bConnected = false;
	bBinded = false;
}

SensorData::SensorData(FString sensorId)
{
	Data.SensorID = sensorId;
	SensorData();
}

SensorData::~SensorData()
{
}

FVector SensorData::GetMyOriginalRotaion()
{
	MyRotationEuler = Data.Euler;

	return MyRotationEuler;
}

void SensorData::SetSensorValue(FSensorInfoStruct & parseData)
{
	Data = parseData;
	bConnected = true;
}

bool SensorData::SetBindingAngle()
{
	if (!bConnected)
		return false;

	// 현 위치를 0으로 맞춰주기위함.
	BindYaw = -Data.Euler.Z;
	bBinded = true;
	return bBinded;
}

FVector SensorData::GetAngle() {
	FVector RetVal;
	RetVal = Data.Euler;
	RetVal.Z += BindYaw;
	return RetVal;
}

bool SensorData::IsBinded() 
{ 
	return bBinded; 
}

