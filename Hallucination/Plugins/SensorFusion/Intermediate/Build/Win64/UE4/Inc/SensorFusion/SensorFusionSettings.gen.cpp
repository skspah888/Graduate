// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SensorFusion/Public/SensorFusionSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSensorFusionSettings() {}
// Cross Module References
	SENSORFUSION_API UClass* Z_Construct_UClass_USensorFusionSettings_NoRegister();
	SENSORFUSION_API UClass* Z_Construct_UClass_USensorFusionSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_SensorFusion();
// End Cross Module References
	void USensorFusionSettings::StaticRegisterNativesUSensorFusionSettings()
	{
	}
	UClass* Z_Construct_UClass_USensorFusionSettings_NoRegister()
	{
		return USensorFusionSettings::StaticClass();
	}
	struct Z_Construct_UClass_USensorFusionSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sensors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sensors;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Sensors_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USensorFusionSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_SensorFusion,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USensorFusionSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SensorFusionSettings.h" },
		{ "ModuleRelativePath", "Public/SensorFusionSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USensorFusionSettings_Statics::NewProp_Sensors_MetaData[] = {
		{ "Category", "Custom" },
		{ "ModuleRelativePath", "Public/SensorFusionSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USensorFusionSettings_Statics::NewProp_Sensors = { "Sensors", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USensorFusionSettings, Sensors), METADATA_PARAMS(Z_Construct_UClass_USensorFusionSettings_Statics::NewProp_Sensors_MetaData, ARRAY_COUNT(Z_Construct_UClass_USensorFusionSettings_Statics::NewProp_Sensors_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USensorFusionSettings_Statics::NewProp_Sensors_Inner = { "Sensors", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USensorFusionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USensorFusionSettings_Statics::NewProp_Sensors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USensorFusionSettings_Statics::NewProp_Sensors_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USensorFusionSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USensorFusionSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USensorFusionSettings_Statics::ClassParams = {
		&USensorFusionSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USensorFusionSettings_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_USensorFusionSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_USensorFusionSettings_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_USensorFusionSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USensorFusionSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USensorFusionSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USensorFusionSettings, 3032310888);
	template<> SENSORFUSION_API UClass* StaticClass<USensorFusionSettings>()
	{
		return USensorFusionSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USensorFusionSettings(Z_Construct_UClass_USensorFusionSettings, &USensorFusionSettings::StaticClass, TEXT("/Script/SensorFusion"), TEXT("USensorFusionSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USensorFusionSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
