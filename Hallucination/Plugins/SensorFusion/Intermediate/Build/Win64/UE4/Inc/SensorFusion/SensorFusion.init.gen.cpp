// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSensorFusion_init() {}
	SENSORFUSION_API UFunction* Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature();
	SENSORFUSION_API UFunction* Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature();
	SENSORFUSION_API UFunction* Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature();
	SENSORFUSION_API UFunction* Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_SensorFusion()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/SensorFusion",
				SingletonFuncArray,
				ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xE3588BC3,
				0x9A7496AD,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
