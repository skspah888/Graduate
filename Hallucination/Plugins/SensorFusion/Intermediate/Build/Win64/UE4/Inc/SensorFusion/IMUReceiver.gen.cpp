// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SensorFusion/Public/IMUReceiver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIMUReceiver() {}
// Cross Module References
	SENSORFUSION_API UFunction* Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_SensorFusion();
	SENSORFUSION_API UFunction* Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	SENSORFUSION_API UFunction* Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature();
	SENSORFUSION_API UFunction* Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	SENSORFUSION_API UClass* Z_Construct_UClass_UIMUReceiver_NoRegister();
	SENSORFUSION_API UClass* Z_Construct_UClass_UIMUReceiver();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics
	{
		struct _Script_SensorFusion_eventBatteryReceivedDelegate_Parms
		{
			int32 Battery;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Battery;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::NewProp_Battery = { "Battery", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_SensorFusion_eventBatteryReceivedDelegate_Parms, Battery), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::NewProp_Battery,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SensorFusion, nullptr, "BatteryReceivedDelegate__DelegateSignature", sizeof(_Script_SensorFusion_eventBatteryReceivedDelegate_Parms), Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics
	{
		struct _Script_SensorFusion_eventGyroReceivedDelegate_Parms
		{
			FVector Gyro;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Gyro_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Gyro;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::NewProp_Gyro_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::NewProp_Gyro = { "Gyro", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_SensorFusion_eventGyroReceivedDelegate_Parms, Gyro), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::NewProp_Gyro_MetaData, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::NewProp_Gyro_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::NewProp_Gyro,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SensorFusion, nullptr, "GyroReceivedDelegate__DelegateSignature", sizeof(_Script_SensorFusion_eventGyroReceivedDelegate_Parms), Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics
	{
		struct _Script_SensorFusion_eventAccelerationReceivedDelegate_Parms
		{
			FVector Acceleration;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Acceleration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Acceleration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::NewProp_Acceleration_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::NewProp_Acceleration = { "Acceleration", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_SensorFusion_eventAccelerationReceivedDelegate_Parms, Acceleration), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::NewProp_Acceleration_MetaData, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::NewProp_Acceleration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::NewProp_Acceleration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SensorFusion, nullptr, "AccelerationReceivedDelegate__DelegateSignature", sizeof(_Script_SensorFusion_eventAccelerationReceivedDelegate_Parms), Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics
	{
		struct _Script_SensorFusion_eventRotationReceivedDelegate_Parms
		{
			FRotator Rotation;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::NewProp_Rotation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_SensorFusion_eventRotationReceivedDelegate_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::NewProp_Rotation_MetaData, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::NewProp_Rotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::NewProp_Rotation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SensorFusion, nullptr, "RotationReceivedDelegate__DelegateSignature", sizeof(_Script_SensorFusion_eventRotationReceivedDelegate_Parms), Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UIMUReceiver::StaticRegisterNativesUIMUReceiver()
	{
	}
	UClass* Z_Construct_UClass_UIMUReceiver_NoRegister()
	{
		return UIMUReceiver::StaticClass();
	}
	struct Z_Construct_UClass_UIMUReceiver_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Battery_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Battery;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Gyro_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Gyro;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Acceleration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Acceleration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReaderFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReaderFilename;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableCSVReader_MetaData[];
#endif
		static void NewProp_bEnableCSVReader_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableCSVReader;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecordingRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RecordingRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecorderFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RecorderFilename;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableCSVRecording_MetaData[];
#endif
		static void NewProp_bEnableCSVRecording_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableCSVRecording;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DebugLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseRawData_MetaData[];
#endif
		static void NewProp_bUseRawData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseRawData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IMU_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IMU_Id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BatteryReceived_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_BatteryReceived;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GyroReceived_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_GyroReceived;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccelerationReceived_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_AccelerationReceived;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationReceived_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_RotationReceived;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UIMUReceiver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_SensorFusion,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "IMUReceiver.h" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Battery_MetaData[] = {
		{ "Category", "Sensor Value" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Battery = { "Battery", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, Battery), METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Battery_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Battery_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Gyro_MetaData[] = {
		{ "Category", "Sensor Value" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Gyro = { "Gyro", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, Gyro), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Gyro_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Gyro_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Acceleration_MetaData[] = {
		{ "Category", "Sensor Value" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Acceleration = { "Acceleration", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, Acceleration), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Acceleration_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Acceleration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "Sensor Value" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Rotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_ReaderFilename_MetaData[] = {
		{ "Category", "CSV Input" },
		{ "EditCondition", "bEnableCSVReader" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_ReaderFilename = { "ReaderFilename", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, ReaderFilename), METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_ReaderFilename_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_ReaderFilename_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVReader_MetaData[] = {
		{ "Category", "CSV Input" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
		{ "ToolTip", "CSV READER" },
	};
#endif
	void Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVReader_SetBit(void* Obj)
	{
		((UIMUReceiver*)Obj)->bEnableCSVReader = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVReader = { "bEnableCSVReader", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UIMUReceiver), &Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVReader_SetBit, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVReader_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVReader_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecordingRate_MetaData[] = {
		{ "Category", "CSV Output" },
		{ "EditCondition", "bEnableCSVRecording" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecordingRate = { "RecordingRate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, RecordingRate), METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecordingRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecordingRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecorderFilename_MetaData[] = {
		{ "Category", "CSV Output" },
		{ "EditCondition", "bEnableCSVRecording" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecorderFilename = { "RecorderFilename", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, RecorderFilename), METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecorderFilename_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecorderFilename_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVRecording_MetaData[] = {
		{ "Category", "CSV Output" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
		{ "ToolTip", "CSV RECORDER" },
	};
#endif
	void Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVRecording_SetBit(void* Obj)
	{
		((UIMUReceiver*)Obj)->bEnableCSVRecording = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVRecording = { "bEnableCSVRecording", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UIMUReceiver), &Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVRecording_SetBit, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVRecording_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVRecording_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_DebugLevel_MetaData[] = {
		{ "Category", "IMU Receiver" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
		{ "ToolTip", "Debugging Level\n0/Default: No Debugging Logs\n1: IMU Sensor Data Logs\n2: IMU Acceleration Direction Motion Logs" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_DebugLevel = { "DebugLevel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, DebugLevel), METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_DebugLevel_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_DebugLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bUseRawData_MetaData[] = {
		{ "Category", "IMU Receiver" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	void Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bUseRawData_SetBit(void* Obj)
	{
		((UIMUReceiver*)Obj)->bUseRawData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bUseRawData = { "bUseRawData", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UIMUReceiver), &Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bUseRawData_SetBit, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bUseRawData_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bUseRawData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_IMU_Id_MetaData[] = {
		{ "Category", "IMU Receiver" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_IMU_Id = { "IMU_Id", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, IMU_Id), METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_IMU_Id_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_IMU_Id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_BatteryReceived_MetaData[] = {
		{ "Category", "IMU Receiver" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_BatteryReceived = { "BatteryReceived", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, BatteryReceived), Z_Construct_UDelegateFunction_SensorFusion_BatteryReceivedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_BatteryReceived_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_BatteryReceived_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_GyroReceived_MetaData[] = {
		{ "Category", "IMU Receiver" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_GyroReceived = { "GyroReceived", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, GyroReceived), Z_Construct_UDelegateFunction_SensorFusion_GyroReceivedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_GyroReceived_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_GyroReceived_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_AccelerationReceived_MetaData[] = {
		{ "Category", "IMU Receiver" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_AccelerationReceived = { "AccelerationReceived", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, AccelerationReceived), Z_Construct_UDelegateFunction_SensorFusion_AccelerationReceivedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_AccelerationReceived_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_AccelerationReceived_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RotationReceived_MetaData[] = {
		{ "Category", "IMU Receiver" },
		{ "ModuleRelativePath", "Public/IMUReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RotationReceived = { "RotationReceived", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIMUReceiver, RotationReceived), Z_Construct_UDelegateFunction_SensorFusion_RotationReceivedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RotationReceived_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RotationReceived_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UIMUReceiver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Battery,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Gyro,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Acceleration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_ReaderFilename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVReader,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecordingRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RecorderFilename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bEnableCSVRecording,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_DebugLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_bUseRawData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_IMU_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_BatteryReceived,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_GyroReceived,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_AccelerationReceived,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIMUReceiver_Statics::NewProp_RotationReceived,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UIMUReceiver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UIMUReceiver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UIMUReceiver_Statics::ClassParams = {
		&UIMUReceiver::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UIMUReceiver_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UIMUReceiver_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UIMUReceiver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UIMUReceiver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UIMUReceiver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UIMUReceiver, 3780245961);
	template<> SENSORFUSION_API UClass* StaticClass<UIMUReceiver>()
	{
		return UIMUReceiver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UIMUReceiver(Z_Construct_UClass_UIMUReceiver, &UIMUReceiver::StaticClass, TEXT("/Script/SensorFusion"), TEXT("UIMUReceiver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UIMUReceiver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
